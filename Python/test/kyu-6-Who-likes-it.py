def likes(names):
    length = len(names)
    result = ''
    if length >= 4:
        result = '{}, {} and {numb} others like this'.format(*names, numb=len(names) - 2)
    elif length == 3:
        result = '{}, {} and {} like this'.format(*names)
    elif length == 2:
        result = '{} and {} like this'.format(*names)
    elif length == 1:
        result = '{} likes this'.format(*names)
    elif not names:
        result = 'no one likes this'
    return result


print(likes(['Alex', 'Max', 'Kate', 'John']))
print(likes(['Alex', 'Max', 'Kate']))
print(likes(['Alex', 'Max']))
print(likes(['Alex']))
print(likes([]))

def likes(names):
    n = len(names)
    return {
        0: 'no one likes this',
        1: '{} likes this', 
        2: '{} and {} like this', 
        3: '{}, {} and {} like this', 
        4: '{}, {} and {others} others like this'
    }[min(4, n)].format(*names[:3], others=n-2)