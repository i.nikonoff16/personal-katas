def likes(names):
    length = len(names)
    result = ''
    if length >= 4:
        result = '{}, {} and {numb} others like this'.format(*names, numb=len(names) - 2)
    elif length == 3:
        result = '{}, {} and {} like this'.format(*names)
    elif length == 2:
        result = '{} and {} like this'.format(*names)
    elif length == 1:
        result = '{} likes this'.format(*names)
    elif not names:
        result = 'no one likes this'
    return result


print(likes(['Alex', 'Max', 'Kate', 'John']))
print(likes(['Alex', 'Max', 'Kate']))
print(likes(['Alex', 'Max']))
print(likes(['Alex']))
print(likes([]))

def likes(names):
    n = len(names)
    return {
        0: 'no one likes this',
        1: '{} likes this', 
        2: '{} and {} like this', 
        3: '{}, {} and {} like this', 
        4: '{}, {} and {others} others like this'
    }[min(4, n)].format(*names[:3], others=n-2)


def test():
    print("New keyboard!!!")

if __name__ == "__main__":
    test()
    """
    Пожалуй это лучшая клавиатура из безпроводных, что у меня была. Буду активно использовать, а также планирую купить такую же на работу,
    так как единообразие опыта набора текста очень важно. рррррпааппррп
    """

    Формальный отчет о нагрузке на ГС за 2021-03-14 21:56:45.752409. 
Все задачи - В
В работе - Р
Приоритетные - С
==================  ===  ===  ===
Сотрудник             В    Р    С
==================  ===  ===  ===
Мартынов Никита      29    5    1
Якимова Любовь        8    3    0
Докалова Екатерина    4    4    0
Лусников Кирилл       8    5    2
Предеина Олеся        9    1    0
Качесов Сергей       16    6    0
Шульман Егор          9    3    0
Кряжевских Андрей    28    5    0
Тюменцев Максим      18    6    0
Осипов Виктор        17    7    1
Шохирев Артем        16    6    0
Мазалов Кирилл       10    2    0
Прокопьев Артем      19    5    0
Кургурцева Алина     12    8    0
Шакиров Руслан        1    1    0
==================  ===  ===  ===