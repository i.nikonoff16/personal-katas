public class ReverseWords {
    public static void main(String[] args) {
        String example = "This is an example!";
        String result = reverseWords(example);

        System.out.println(result);

    }
    public static String reverseWords(final String original) {
        /**
         * Зеркально переворачиваем подстроки из строки. Единица разбиения - строка, отделеная пробелом.
         */
        String[] originalArray = original.split("\\s+");

        for (int i = 0; i < originalArray.length; i++) {
            String word = originalArray[i];
            StringBuilder rawWord = new StringBuilder(word);

            rawWord.reverse();
            word = rawWord.toString();

            originalArray[i] = word;
        }

        String resultStr = String.join(" ", originalArray);

        return resultStr;
    }
}
