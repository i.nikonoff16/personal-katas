package ch01.sec01;

import java.util.Scanner;

public class NumRepresentation {
    /**
     * Задание 1, глава 1
     * 
     * Напишите программу, вводящую целочисленное значение и выводящую его в двоичной, восьмеричной и 
     * шестнадцатиричной форме.
     * Организуйте вывод обратного значения в виде шестнадцатиричного числа с плавающей точкой.
     */

     public static void main (String[] args) {
         Scanner scanner = new Scanner(System.in);
         int originalNum = scanner.nextInt();
         scanner.close();

         // Представление числа в разных системах счисления
         System.out.println("\nBinary Integer representation: " + Integer.toBinaryString(originalNum));
         System.out.println("Octal Integer representation: " + Integer.toOctalString(originalNum));
         System.out.println("Hex Integer representation: " + Integer.toHexString(originalNum));

         // Перевод числа в вещественное и представление его в шестнадцатиричном формате
         double produceNum = (double) originalNum;
         System.out.println("\nHex Double representation: " + Double.toHexString(produceNum));
     }
}
