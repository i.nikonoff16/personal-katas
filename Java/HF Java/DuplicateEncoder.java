public class DuplicateEncoder {
    public static void main (String[] args) {
        String word = "Prespecialized";

        word = encode(word);
        System.out.println(word);
    }

    static String encode(String word) {
        word = word.toLowerCase();
        boolean isMoreThanOne;

        char[] endArr = new char[word.length()];
        char[] startArr = new char[word.length()];
        startArr = word.toCharArray();

        for (int i = 0; i < word.length(); i++) {
            isMoreThanOne = false;
            int count = 0;
            for (int j = 0; j < word.length(); j++) {
            
                if (startArr[i] == startArr[j]){
                    count++;
                }
                if (count > 1) {
                    isMoreThanOne = true;
                }
            }
            if (isMoreThanOne) {
                endArr[i] = ')';
            }
            else {endArr[i] = '(';}  
        }
        String result = new String(endArr);
        return result;
    }
}
