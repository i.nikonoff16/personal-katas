package ch05.ex02;

import ch05.ex01.SimpleDotCom;

import java.util.ArrayList;

public class DotComTestDrive {
    public static void main (String[] args) {
        DotCom dot = new DotCom();

        ArrayList<String> locations = new ArrayList<>();
        locations.add("1");
        locations.add("2");
        locations.add("3");

        dot.setLocationCells(locations);

        String userGuess = "2";
        String result = dot.checkYourself(userGuess);

        String testResult = "Неудача";

        if (result.equals("Попал")) {
            testResult = "Пройден";
        }

        System.out.println(testResult);
    }
}
