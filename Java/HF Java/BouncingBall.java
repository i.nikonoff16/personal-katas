public class BouncingBall {
    public static void main (String[] args) {
        int result = bouncingBall(3, 1.0, 1.5);

        System.out.println(result);
    }

    public static int bouncingBall(double h, double bounce, double window) {
        int result = 0;
	    if (h <= 0 || bounce < 0 || bounce >= 1 || window >= h) {
            result = -1;
        }
        else {
            while (h > window) {
                result++;
                h = h * bounce;

                if (h > window) {
                    result++;
                }
            }
        }
        return result;
    }
}
