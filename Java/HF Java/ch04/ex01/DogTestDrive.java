package ch04.ex01;

public class DogTestDrive {
    public static void main (String[] args) {
        Dog one = new Dog();
        one.setSize(70);

        Dog two = new Dog();
        two.setSize(8);

        Dog three = new Dog();
        three.setSize(35);;

        one.bark(1);
        two.bark(3);
        three.bark(2);

        Dog[] pets;
        pets = new Dog[7];

        pets[0] = new Dog();
        pets[1] = new Dog();

        pets[0].setSize(30);
        int x = pets[0].getSize();

        System.out.println(x);
    }
}
