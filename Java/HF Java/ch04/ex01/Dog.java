package ch04.ex01;

public class Dog {
    private int size;
    private String name;

    public void setSize(int sizeOfDog) {
        size = sizeOfDog;
    }

    public int getSize() {
        return size;
    }

    public void setName(String nameOfDog) {
        name = nameOfDog;
    }

    public String getName() {
        return name;
    }

    public void bark(int numOfBarks) {
        while (numOfBarks > 0) {
            if (size > 60) {
                System.out.println("Гав, гав!");
            } else if (size > 14) {
                System.out.println("Вуф, вуф!");
            } else {
                System.out.println("Тяф, тяф!");
            }
            numOfBarks = numOfBarks - 1;
        }
    }
}
