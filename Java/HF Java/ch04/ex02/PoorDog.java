package ch04.ex02;

public class PoorDog {
    
    private int size;
    private String name;

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}
