package ch04.ex03;

public class Robot {
    int x=0;
    int y=0;

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    Direction direction = Direction.UP;

    public Direction getDirection() {
        return direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void turnLeft() {
        System.out.println("Поворот против часовой стрелки");
        if (direction == Direction.DOWN){
            this.direction = Direction.RIGHT;
            return;
        }
       
        if (direction == Direction.UP){
            this.direction = Direction.LEFT;
            return;
        }
       
        if (direction == Direction.LEFT){
            this.direction = Direction.DOWN;
            return;
        }
       
        if (direction == Direction.RIGHT){
            this.direction = Direction.UP;
            return;
        }
    }

    public void turnRight() {
        System.out.println("поворот по часовой стрелке");
        if (this.direction == Direction.DOWN){
            System.out.println("Вниз -> влево");
            this.direction = Direction.LEFT;
            return;
        }
       
        if (this.direction == Direction.UP){
            System.out.println("Вверх -> вправо");
            this.direction = Direction.RIGHT;
            return;
        }
       
        if (this.direction == Direction.LEFT){
            System.out.println("Влево -> вверх");
            this.direction = Direction.UP;
            return;
        }
       
        if (this.direction == Direction.RIGHT){
            System.out.println("Вправо -> вниз");
            this.direction = Direction.DOWN;
            return;
        }
    }

    public void stepForward() {
        System.out.println("движение");
        if (direction == Direction.DOWN){
            System.out.println("вниз");
            this.y--;
        }
       
        if (direction == Direction.UP){
            System.out.println("вверх");
            this.y++;
        }
       
        if (direction == Direction.LEFT){
            System.out.println("налево");
            this.x--;
        }
       
        if (direction == Direction.RIGHT){
            System.out.println("направо");
            this.x++;
        }
    }
    public static void moveRobot(Robot robot, int toX, int toY) {
        int currentY = robot.getY();
        int currentIntVector = toY - currentY;
        Direction directionY;

        if (currentIntVector > 0) {
            directionY = Direction.UP;
        } else if (currentIntVector < 0) {
            directionY = Direction.DOWN;
        } else {
            directionY = robot.getDirection();
        }
        currentIntVector = Math.abs(currentIntVector);

        while (true) {
            if (directionY == robot.getDirection()) {
                break;
            } else {
                robot.turnRight();
            }
        }

        while (currentIntVector > 0) {
            robot.stepForward();
            currentIntVector--;
        }

        int currentX = robot.getX();
        currentIntVector = toX - currentX;
        Direction directionX;

        if (currentIntVector > 0) {
            directionX = Direction.RIGHT;
        } else if (currentIntVector < 0) {
            directionX = Direction.LEFT;
        } else {
            directionX = robot.getDirection();
        }
        currentIntVector = Math.abs(currentIntVector);

        while (true) {
            if (directionX == robot.getDirection()) {
                break;
            } else {
                robot.turnRight();
            }
        }

        while (currentIntVector > 0) {
            robot.stepForward();
            currentIntVector--;
        }
    }
}
