import java.util.Arrays;

public class TwoToOne {
    public static void main (String[] args) {
        String one = "xyaabbbccccdefww";
        String two = "xxxxyyyyabklmopq";

        String best = longest(one, two);

        System.out.println(best);
    }
    public static String longest (String s1, String s2) {
        String concated = s1 + s2;
        String result = filteredAndSorted(concated);
        return result;
    }
    public static String filteredAndSorted (String row) {
        char[] charRepr = row.toCharArray();
        Arrays.sort(charRepr);
        char[] charSet = new char[row.length()];

        char buff = ' ';

        for (int i = 0; i < charRepr.length; i++) {
            if (buff == charRepr[i]){
                charSet[i] = ' ';
            }
            else {
                buff = charRepr[i];
                charSet[i] = charRepr[i];
            }
        }

        String result = new String(charSet);
        result = result.replace(" ", "");

        return result;
    }
}
