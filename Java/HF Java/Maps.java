public class Maps {
    public static void main (String[] args) {
        int[] arr = {1, 2, 3};

        int[] result = map(arr);

        for (int i : result){
            System.out.println(i);
        }
    }
    public static int[] map(int[] arr) {
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i] * 2;
        }

        return result;
    }
}
