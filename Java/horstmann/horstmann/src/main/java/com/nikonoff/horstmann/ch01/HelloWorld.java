package com.nikonoff.horstmann.ch01;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!".length());
        
        Random rnd = new Random();
        System.out.println(rnd.nextDouble());
        
        System.out.println(Long.MAX_VALUE + " " + Double.MIN_VALUE);
        
        int hexNum = 0xff;
        System.out.println("Previously this number was in hex, but now...: " + hexNum);
        
        /* Check constants */
        boolean leapYear = rnd.nextBoolean();
        final int DAYS_IN_FEBRUARY;

        if (leapYear) {
            DAYS_IN_FEBRUARY = 29;
        } else {
            DAYS_IN_FEBRUARY = 28;
        }

        System.out.println("Days in February this year are " + DAYS_IN_FEBRUARY);

        double x = 3.75;
        int n = (int) Math.round(x);
        System.out.println(n);
    
        boolean test = (0 <= n && n < 20);

        BigInteger k = new BigInteger("9876543210123456789");
        BigInteger s = BigInteger.valueOf(1234567890987654321L);
        
        System.out.println("That's both Big Integers: \n" + k + "\n" + s);
        System.out.println(k.add(s));
        System.out.println(k);
        
        k = k.add(s);
        BigDecimal z = BigDecimal.valueOf(2, 0).subtract(BigDecimal.valueOf(11, 1));
        System.out.println(z);

    }
}
