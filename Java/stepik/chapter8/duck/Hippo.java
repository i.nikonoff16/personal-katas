package stepik.chapter8.duck;

public class Hippo extends Animal{
    public Hippo() {
        System.out.println("Hippo created");
    }
}

class Animal {
    public Animal() {
        System.out.println("Animal created");
    }
}


