package stepik.chapter8.duck;

public class Duck {
    private int pounds = 6;
    private float floatability = 2.1F;
    private String name = "Universal";
    private long[] feathers = {1,2,3,4,5,6,7};
    private boolean canFly = true;
    private int maxSpeed = 25;

    public Duck() {
        System.out.println("Duck Mark I");
    }

    public Duck(boolean canFly) {
        this.canFly = canFly;
        System.out.println("Duck Mark II");
    }

    public Duck(String name, long[] feathers) {
        this.name = name;
        this.feathers = feathers;
        System.out.println("Duck Mark III");
    }

    public Duck(int pounds, float floatability) {
        this.pounds = pounds;
        this.floatability = floatability;
        System.out.println("Duck Mark IV");
    }

    public Duck(float floatability, int maxSpeed) {
        this.floatability = floatability;
        this.maxSpeed = maxSpeed;
        System.out.println("Duck Mark V");
    }

    public float getFloatability() {
        return floatability;
    }

    public void setFloatability(float floatability) {
        this.floatability = floatability;
    }

    public int getPounds() {
        return pounds;
    }

    public void setPounds(int pounds) {
        this.pounds = pounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long[] getFeathers() {
        return feathers;
    }

    public void setFeathers(long[] feathers) {
        this.feathers = feathers;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
