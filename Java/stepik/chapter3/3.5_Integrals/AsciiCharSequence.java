import java.util.Arrays;


public class AsciiCharSequence implements CharSequence {

    private final byte[] asciiString;

    public AsciiCharSequence(byte[] byteSequence) {
        asciiString = byteSequence.clone();
    }

    @Override
    public String toString() {
        /* 
        Returns a string containing the characters in this sequence in the same order as this sequence. 
        The length of the string will be the length of this sequence.
        Overrides:
        toString in class Object
        Returns:
        a string consisting of exactly this sequence of characters
        */    
        return new String(asciiString);
    }

    @Override
    public int length() {
        /*       
        Returns the length of this ascii sequence. The length is the number of 8-bit bytes in the sequence.
        Returns:
        the number of bytes in this sequence
        */
        return asciiString.length;
    }

    @Override
    public char charAt(int index) {
        /* 
        Returns the char value at the specified index. An index ranges from zero to length() - 1. 
        The first char value of the sequence is at index zero, the next at index one, and so on, as for array indexing.
        If the char value specified by the index is a surrogate, the surrogate value is returned.

        Parameters:
        index - the index of the byte value to be returned
        Returns:
        the specified char value
        */
        byte byteCharRepresentation = asciiString[index];

        return (char) byteCharRepresentation;
    }

    @Override
    public AsciiCharSequence subSequence(int start, int end) {
        /* 
        Returns a CharSequence that is a subsequence of this sequence. The subsequence starts with the char value at the specified index and ends with the char value at index end - 1. The length (in chars) of the returned sequence is end - start, so if start == end then an empty sequence is returned.
        Parameters:
        start - the start index, inclusive
        end - the end index, exclusive
        Returns:
        the specified subsequence
        */

        byte[] newByteSequence = Arrays.copyOfRange(asciiString, start, end);
        AsciiCharSequence newAsciiSequence = new AsciiCharSequence(newByteSequence);
        
        return newAsciiSequence;
    }
}
