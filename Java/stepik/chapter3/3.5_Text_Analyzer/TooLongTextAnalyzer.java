public class TooLongTextAnalyzer implements TextAnalyzer {
    private int maxLenght;

    public TooLongTextAnalyzer(int number) {
        maxLenght = number;
    }

    public Label processText(String text) {
        if (text.length() > maxLenght) {
            return Label.TOO_LONG;
        } else {
            return Label.OK;
        }
    }
}
