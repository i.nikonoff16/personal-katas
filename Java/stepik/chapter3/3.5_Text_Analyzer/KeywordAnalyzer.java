public abstract class KeywordAnalyzer implements TextAnalyzer{
    protected abstract String[] getKeywords();

    protected abstract Label getLabel();

    @Override
    public Label processText(String text) {
        String[] keywords = getKeywords();

        Label result = Label.OK;
        for (String word : keywords) {
            if (text.contains(word)) {
                result = getLabel();
                break;
            };
        };
        return result;
    }
}
